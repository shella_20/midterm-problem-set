import java.util.Scanner;

public class GameSimulationClient {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args){

        /*
         * The first part is to show the prerequisite input for spawning three.character
         */
        System.out.println("Choose your desired class (type the class name):");
        System.out.println("1. Archer 2. Assasin 3. Barbarian");
        System.out.println("4. Cleric 5. Knight 6. Mage");
        String chosenCharacter = sc.nextLine();
        System.out.println("Type the name of your Character...");
        String characterName = sc.nextLine();
        Character aCharacter = null;
        String responseEquipLeftHandWeapon = "";
        String responseEquipRightHandWeapon = "";
        /* End of the first part*/

        /*
         * The second part is a logic for creating various three.character and set default weapon whenever new three.character spawned.
         */
        if(chosenCharacter.equalsIgnoreCase("archer")){
            Archer archer = new Archer(characterName);
            Bow bow = new Bow("Cupid Bow",5, 1, 10);
            responseEquipLeftHandWeapon = archer.setLeftWeapon(bow);
            aCharacter = archer;
        }else if (chosenCharacter.equalsIgnoreCase("assasin")){
            Assasin assasin = new Assasin(characterName);
            Knife ebonyKnife = new Knife("Ebony Knife",7, 2, 7);
            Knife ivoryKnife = new Knife("Ivory Knife",7, 2, 7);
            responseEquipLeftHandWeapon = assasin.setLeftWeapon(ebonyKnife);
            responseEquipRightHandWeapon = assasin.setRightWeapon(ivoryKnife);
            aCharacter = assasin;
        }else if (chosenCharacter.equalsIgnoreCase("barbarian")){
            Barbarian barbarian = new Barbarian(characterName);
            Broadsword agni = new Broadsword("Agni",15, 0, 1);
            Broadsword rudra = new Broadsword("Rudra",15, 0, 1);
            responseEquipLeftHandWeapon = barbarian.setLeftWeapon(agni);
            responseEquipRightHandWeapon = barbarian.setRightWeapon(rudra);
            aCharacter = barbarian;
        }else if (chosenCharacter.equalsIgnoreCase("cleric")){
            Cleric cleric = new Cleric(characterName);
            Shield angelShield = new Shield("Angel Shield",1, 6, 9);
            Mace angelMace = new Mace("Angel Mace", 4, 2, 10);
            responseEquipLeftHandWeapon = cleric.setLeftWeapon(angelShield);
            responseEquipRightHandWeapon = cleric.setRightWeapon(angelMace);
            aCharacter = cleric;
        }else if (chosenCharacter.equalsIgnoreCase("knight")){
            Knight knight = new Knight(characterName);
            Shield kiteShield = new Shield("Kite Shield", 0, 8, 8);
            Broadsword claymore = new Broadsword("Claymore", 10, 3, 3);
            responseEquipLeftHandWeapon = knight.setLeftWeapon(kiteShield);
            responseEquipRightHandWeapon = knight.setRightWeapon(claymore);
            aCharacter = knight;
        }else if (chosenCharacter.equalsIgnoreCase("mage")){
            Mage mage = new Mage(characterName);
            WizardStaff grandMageStaff = new WizardStaff("Grand Mage Staff", 1, 1, 14);
            responseEquipLeftHandWeapon = mage.setLeftWeapon(grandMageStaff);
            aCharacter = mage;
        }else{
            System.out.println("Your selected class currently unavailable. The program will exit...");
            System.exit(0);
        }

        System.out.println("Your Character successfully spawned. Here is the bio...");
        System.out.println(aCharacter);
        System.out.println(responseEquipLeftHandWeapon);
        System.out.println(responseEquipRightHandWeapon);
        /* End of second part */

        /*
         * The third part is the chance for user to customize the equip of their three.character.
         */

        boolean isFinished = false;
        while(!isFinished){
            System.out.println("Next step:");
            System.out.println("1. Create and Equip new Left Hand Weapon");
            System.out.println("2. Create and Equip new Right Hand Weapon");
            System.out.println("3. Drop Left Hand Weapon");
            System.out.println("4. Drop Right Hand Weapon");
            System.out.println("5. Finish Customization");
            try{
                int selectedChoice = Integer.parseInt(sc.nextLine());
                if(selectedChoice == 1){
                    aCharacter = createWeaponBasedOnChoice(selectedChoice, aCharacter);
                }else if(selectedChoice == 2){
                    aCharacter = createWeaponBasedOnChoice(selectedChoice, aCharacter);
                }else if(selectedChoice == 3){
                    if(chosenCharacter.equalsIgnoreCase("archer")) {
                        Archer archer = new Archer(characterName);
                        archer.dropLeftWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("assasin")) {
                        Assasin assasin = new Assasin(characterName);
                        assasin.dropLeftWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("barbarian")) {
                        Barbarian barbarian = new Barbarian(characterName);
                        barbarian.dropLeftWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("cleric")) {
                        Cleric cleric = new Cleric(characterName);
                        cleric.dropLeftWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("knight")) {
                        Knight knight = new Knight(characterName);
                        knight.dropLeftWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("mage")) {
                        Mage mage = new Mage(characterName);
                        mage.dropLeftWeapon();
                    }
                }else if(selectedChoice == 4){
                    if(chosenCharacter.equalsIgnoreCase("archer")) {
                        Archer archer = new Archer(characterName);
                        archer.dropRightWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("assasin")) {
                        Assasin assasin = new Assasin(characterName);
                        assasin.dropRightWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("barbarian")) {
                        Barbarian barbarian = new Barbarian(characterName);
                        barbarian.dropRightWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("cleric")) {
                        Cleric cleric = new Cleric(characterName);
                        cleric.dropRightWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("knight")) {
                        Knight knight = new Knight(characterName);
                        knight.dropRightWeapon();
                    }else if(chosenCharacter.equalsIgnoreCase("mage")) {
                        Mage mage = new Mage(characterName);
                        mage.dropRightWeapon();
                    }
                }else if(selectedChoice == 5){
                    isFinished = true;
                }else{
                    System.out.println("Choices only 1-5. Others will be ignored");
                }
            }catch(NumberFormatException e){
                System.out.println("This choices only use numbers. Other will be ignored");
            }
        }
        /* End of third part */

        System.out.println("Your Character successfully spawned. Here is the bio...");
        System.out.println(aCharacter);
        System.out.println("Enjoy your game!!!");

    }

    public static Character createWeaponBasedOnChoice(int selectedChoice, Character aCharacter){
        System.out.println("Choose your desired weapon...");
        System.out.println("1. Bow 2. Broadsword 3. Crossbow");
        System.out.println("4. Knife 5. Mace 6. Pike");
        System.out.println("7. Shield 8. Spear 9. WizardStaff");
        String chosenWeapon = sc.nextLine();
        System.out.println("Name of Your Weapon?");
        String weaponName = sc.nextLine();


        if(selectedChoice == 1){
            if(chosenWeapon.equalsIgnoreCase("bow")){
                Bow aBow = new Bow(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aBow);
            }else if(chosenWeapon.equalsIgnoreCase("broadsword")){
                Broadsword aBroadsword = new Broadsword(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aBroadsword);
            }else if(chosenWeapon.equalsIgnoreCase("crossbow")){
                Crossbow aCrossbow = new Crossbow(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aCrossbow);
            }else if(chosenWeapon.equalsIgnoreCase("knife")){
                Knife aKnife = new Knife(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aKnife);
            }else if(chosenWeapon.equalsIgnoreCase("mace")){
                Mace aMace = new Mace(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aMace);
            }else if(chosenWeapon.equalsIgnoreCase("pike")){
                Pike aPike = new Pike(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aPike);
            }else if(chosenWeapon.equalsIgnoreCase("shield")){
                Shield aShield = new Shield(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aShield);
            }else if(chosenWeapon.equalsIgnoreCase("spear")){
                Spear aSpear = new Spear(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aSpear);
            }else if(chosenWeapon.equalsIgnoreCase("staff")){
                WizardStaff aWizardStaff = new WizardStaff(weaponName, 5,5,6);
                //TODO Implement, set Left Hand Weapon, Print the response message of equipping the weapon.
                aCharacter.setLeftWeapon(aWizardStaff);
            }else{
                System.out.println("Nothing Happened Here");
            }

        }else if(selectedChoice == 2){
            if(chosenWeapon.equalsIgnoreCase("bow")){
                Bow aBow = new Bow(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else if(chosenWeapon.equalsIgnoreCase("broadsword")){
                Broadsword aBroadsword = new Broadsword(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else if(chosenWeapon.equalsIgnoreCase("crossbow")){
                Crossbow aCrossbow = new Crossbow(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else if(chosenWeapon.equalsIgnoreCase("knife")){
                Knife aKnife = new Knife(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else if(chosenWeapon.equalsIgnoreCase("mace")){
                Mace aMace = new Mace(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else if(chosenWeapon.equalsIgnoreCase("pike")){
                Pike aPike = new Pike(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else if(chosenWeapon.equalsIgnoreCase("shield")){
                Shield aShield = new Shield(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else if(chosenWeapon.equalsIgnoreCase("spear")){
                Spear aSpear = new Spear(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else if(chosenWeapon.equalsIgnoreCase("staff")){
                WizardStaff aWizardStaff = new WizardStaff(weaponName, 5,5,6);
                //TODO Implement, set Right Hand Weapon, Print the response message of equipping the weapon.
            }else{
                System.out.println("Nothing Happened Here");
            }
        }else{
            System.out.println("Nothing happened here!");
        }
        return aCharacter;
    }
}
